<?php

/**
 * Migrate Joomla content
 */

/**
 * JoomlaContentMigration class
 */
class JoomlaContentMigration extends Migration {

  /**
   * constructor: field mapping
   */
  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('joomla'));
    $connection_info = variable_get('joomla_migrate_settings_database', array());
    if (!empty($connection_info)) {
      Database::addConnectionInfo('joomla_migrate', 'default', $connection_info);
    }
    else {
      drupal_set_message(t('Joomla Migrate module database settings not found. Please set the module and go to <a href="@url">Migrate page</a>.', array('@url' => url('admin/content/migrate'))));
      drupal_goto('admin/config/content/joomla_migrate');
    }
    $query = Database::getConnection('default', 'joomla_migrate')
      ->select('jos_content', 'jc')
      ->fields('jc');
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $nodetype = variable_get('joomla_migrate_default_static_nodetype', '');
    $this->destination = new MigrateDestinationNode($nodetype);
    // We instantiate the MigrateMap
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema(),
      'default'
    );
    $text_format = variable_get('joomla_migrate_content_text_format', '');
    // Finally we add simple field mappings
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'fulltext');
    $this->addFieldMapping('body:summary', 'introtext');
    $this->addFieldMapping('body:format')
         ->defaultValue($text_format);
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'modified');
    // if enabled content path migrate, store this
    if (variable_get('joomla_migrate_content_path', FALSE) && module_exists('path')) {
      static $joomla_migrate_static;
      if (!isset($joomla_migrate_static)) {
        $joomla_migrate_static['path'] = &drupal_static('joomla_migrate_path');
      }
      $joomla_migrate_static['path'] = TRUE;
    }
  }

  /**
   * prepare "modified" value
   */
  public function prepareRow($row) {
    if ($row->modified == '0000-00-00 00:00:00') {
      $row->modified = $row->created;
    }
  }

  /**
   * prepare other values
   */
  public function prepare($entity, stdClass $row) {
    /* Joomla content state possible values:
     *  0 - un-published
     *  1 - published
     * -1 - Archived
     * -2 - marked for deletion. Added to trash can, but not yet emptied. */
    switch($row->state) {
      case 0:
        $entity->status = 0;
        break;
      case 1:
        $entity->status = 1;
        break;
      case -1:
        $entity->promote = 0;
        break;
      case -2:
        $entity->status = 0;
        $entity->promote = 0;
        break;
    }
  }

  /**
   * prepare node, after migration
   */
  public function complete($entity, stdClass $row) {
    /**
     * Joomla content path migrate
     * joomla content path: http://example.com/component/content/article/$category_id-$category_alias/$content_id-$content_alias.html
     */
    static $joomla_migrate_static;
    if (!isset($joomla_migrate_static)) {
      $joomla_migrate_static['path'] = &drupal_static('joomla_migrate_path');
    }
    if ($joomla_migrate_static['path']) {
      // get category information
      $result = Database::getConnection('default', 'joomla_migrate')
        ->select('jos_categories', 'jcat')
        ->fields('jcat')
        ->condition('jcat.id', $row->catid)
        ->execute();
      foreach($result as $category) {
        $path = array();
        $path['source'] = 'node/' . $entity->nid;
        $path['alias'] = 'component/content/article/' . $category->id . '-' . $category->alias . '/' . $row->id . '-' . $row->alias . '.html';
        path_save($path);
      }
    }
  }
}
